package jdolly.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class Oracle {

	public static void behaviorchanges(String resultFolderPath) {

		File[] listFiles = Util.getFilesFromPath(resultFolderPath);
		for (File test : listFiles) {
			if (test.isDirectory()) {
				copyBehaviorChangeFilesFromDirectoryToFolderPath(test, resultFolderPath);
			}
		}
	}

	private static void copyBehaviorChangeFilesFromDirectoryToFolderPath(File directory, String resultFolderPath) {
		File[] testFolders = directory.listFiles();
		for (File testFolder : testFolders) {
			if (testFolder.getName().equals("out")) {
				File[] resultsFiles = testFolder.listFiles();
				for (File resultFile : resultsFiles) {
					if (resultFile.getName().equals("BEHAVIORCHANGE_FAILURE")) {
						try {
							tryToCopyFileToResultFolder(resultFolderPath, directory);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private static void tryToCopyFileToResultFolder(String resultFolderPath, File test) throws IOException {
		String pathname = resultFolderPath
				+ "/behaviorchange/"
				+ test.getName();
		copyDirectory(test, new File(pathname));
	}

	public static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				boolean mkdir = targetLocation.mkdir();
				if (mkdir)
					printMsgSuccessfulCopy();
				else
					printMsgProblemWithCopy();
			}
			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				File sourceChildLocation = new File(sourceLocation, children[i]);
				File targetChildLocation = new File(targetLocation, children[i]);
				copyDirectory(sourceChildLocation, targetChildLocation);
			}
		} else {
			// targetLocation.createNewFile();

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			FileUtil.copyTheBitsFromInputStreamToOutputStream(in, out);
			
			in.close();
			out.close();
		}
	}

	private static void printMsgProblemWithCopy() {
		System.out.println("problema ao copiar");
	}

	private static void printMsgSuccessfulCopy() {
		System.out.println("copiado com sucesso");
	}

	public static void categorize(String engine, String resultFolderPath, String problemFileName) {

		Map<String, Integer> problems = new HashMap<String, Integer>();

		File[] listFiles = Util.getFilesFromPath(resultFolderPath);
		for (File test : listFiles) {
			if (test.isDirectory()) {
				File out = new File(test, "out");
				File outEngine = new File(out, engine);
				File target = new File(outEngine, problemFileName);

				//factory method poderia ser aplicado aqui, j� que existem 3 tipos de or�culos.
				if (target.exists()) {
					if (engine.equals(StrUtil.ECLIPSE))
						eclipseOracle(problems, test, target);
					else if (engine.equals(StrUtil.JRRT_REFACTORING_TOOLS)) {						
						jrrtOracle(problems, test, target);						
					} else {
						netbeansOracle(problems,test,target);
					}
				}
			}
		}

		Util.printEachProblemAndHisAmountWithOrWithoutSeparator(problems, "No");
		
		printTotalOfBugs(problems);

	}

	private static void printTotalOfBugs(Map<String, Integer> problems) {
		System.out.println("Total de bugs: " + problems.size());
	}
	
	public static void categorizeAntigo(String resultFolderPath,
			String problemFileName) {

		Map<String, Integer> problems = new HashMap<String, Integer>();

		File[] listFiles = Util.getFilesFromPath(resultFolderPath);
		for (File test : listFiles) {
			if (test.isDirectory()) {
				File out = new File(test, "out");
				File target = new File(out, problemFileName);

				if (target.exists()) {
					eclipseOracle(problems, test, target);					
				}
			}
		}
		
		Util.printEachProblemAndHisAmountWithOrWithoutSeparator(problems, "No");
		
		printTotalOfBugs(problems);

	}
	
	private static void netbeansOracle(Map<String, Integer> problems, File test, File target) {
		try {
			addAllProblemsOfTargetFileToProblems(problems, test, target);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
	}

	private static void addAllProblemsOfTargetFileToProblems(Map<String, Integer> problems, 
			File test, File target)
			throws FileNotFoundException, IOException {
		FileReader in = new FileReader(target);
		BufferedReader br = new BufferedReader(in);
		String actualLineOfTargetFile;
		String problemName = StrUtil.EMPTY_STRING;
		
		while ((actualLineOfTargetFile = br.readLine()) != null) {
			if (actualLineOfTargetFile.contains("^"))
				continue;
			if (!actualLineOfTargetFile.contains("javac"))
				continue;
			if (actualLineOfTargetFile.contains(StrUtil.WARNING))
				continue;
			if (actualLineOfTargetFile.contains("Compiling"))
				continue;
			actualLineOfTargetFile = applyReplacesForMatchedSymbols(actualLineOfTargetFile);
			problemName = problemName + actualLineOfTargetFile + "\n";
			break;
		}
		if (problems.containsKey(problemName)) {
			Integer updatedTotalOfProblems = problems.get(problemName) + 1;
			problems.put(problemName, updatedTotalOfProblems);
		} else {
			problems.put(problemName, 1);
			System.out.println(test);
		}
		in.close();
	}

	private static String applyReplacesForMatchedSymbols(String s) {
		s = s.replaceAll("[.]*:[1-9]+:", "");
		s = s.replaceAll("[.]*.java", "");
		s = s.replaceAll("P[1-2]_0/Class[1-3]_0", "");
		s = s.replaceAll("P[1-2]_0.Class[1-3]_0", "");
		s = s.replaceAll("[a-z]_0\\([a-z]*\\)", "");
		return s;
	}

	private static void jrrtOracle(Map<String, Integer> problems, File test,
			File target) {
		try {
			FileReader in = new FileReader(target);
			BufferedReader br = new BufferedReader(in);
			String s;
			String x = "";
			boolean inclueAproxima = true;
			boolean achouErro = false;

			while ((s = br.readLine()) != null) {
				if (s.contains("^"))
					continue;
				// so conta o 1o. erro
				if (s.contains("----------") && achouErro)
					break;
				if (s.contains("ERROR")) {
					inclueAproxima = true;
					achouErro = true;
				}

				if (inclueAproxima) {
					if (s.contains("WARNING"))
						inclueAproxima = false;
					else {
						if (!s.contains("ERROR")
								&& !s.contains("problem")) {
							//para remover nome de classes e metodos
//							s = s
//									.replaceAll(
//											"[(]?[(]?[a-zA-Z0-9]+_[0-9][(]?[(]?[\\w]*[)]?[\\w]*[)]?",
//											" ");
							//para remover o codigo que ocorre o problmea, deixar so a mensagem
							if (s.contains(";"))
								s = "";
							else
								s = s
								.replaceAll(
										"[(]?[(]?[a-zA-Z0-9]+_[0-9][(]?[(]?[\\w]*[)]?[\\w]*[)]?",
										" ");
								
						} else
							s = "ERROR";
						x = x + "\n" + s;

					}

				}
			}
			if (problems.containsKey(x)) {
				Integer integer = problems.get(x);
				integer = integer + 1;
				problems.put(x, integer);
			} else {
				problems.put(x, 1);
				System.out.println(test);
			}

			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void eclipseOracle(Map<String, Integer> problems, File test,	File target) {
		try {
			tryToGetAllErrors(problems, test, target);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void tryToGetAllErrors(Map<String, Integer> problems, File test, File target)
			throws FileNotFoundException, IOException {
		FileReader in = new FileReader(target);
		BufferedReader br = new BufferedReader(in);
		String s;
		
		
		//pega s� o 1o. erro
//			s = br.readLine();
//			s = s.replaceAll(
//					" [a-zA-Z0-9]+_[0-9][(]?[\\w]*[)]?", " ");
		
		//pega todos os errors
		String x = "";
		while ((s = br.readLine()) != null) {
			s = s.replaceAll(
					" [a-zA-Z0-9]+_[0-9][(]?[\\w]*[)]?", StrUtil.SPACE_STRING);
			x = s;
		}
		if (problems.containsKey(x)) {
			Integer integer = problems.get(x);
			integer = integer + 1;
			problems.put(x, integer);
		} else {
			System.out.println(test);
			
			problems.put(x, 1);
		}

		in.close();
	}

	
	
	public static void categorizeOege(String resultFolderPath) {

		Map<String, Integer> problems = new HashMap<String, Integer>();

		File[] listFiles = Util.getFilesFromPath(resultFolderPath);
		
		for (File test : listFiles) {
			if (test.isDirectory()) {
				File[] testFolders = test.listFiles();
				for (File testFolder : testFolders) {
					if (testFolder.getName().equals(StrUtil.COMPILATION_ERROR)) {
						jrrtOracle(problems, testFolder, testFolder);
					}
				}
			}
		}
		Util.printEachProblemAndHisAmountWithOrWithoutSeparator(problems,"Yes");
	}


	public static void main(String[] args) {

//		 Oracle.categorizeOege("/Users/gustavo/Doutorado/experiments/refactoring-constraints/renamefield/jrrt/");
		// Oracle.behaviorchanges("/Users/gustavo/Mestrado/genExperimentos/genExperimento/pullupmethod/package/");
		Oracle
				.categorize(
						"jrrt",
						"/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/movemethod/last/",
						"POST_REFACTOR_NOT_COMPILE");
		
//		Oracle
//		.categorizeAntigo(				
//				"/Users/gustavo/Doutorado/experiments/refactoring-constraints/addparameter/eclipse/",
//				"POST_REFACTOR_NOT_COMPILE");
	}

}

package jdolly.util;

import java.io.File;
import java.io.FileFilter;

public class NetbeansCounter {

	public static void main(String[] args) {
		runNetbeansCounter();
	}

	private static void runNetbeansCounter() {
		String path = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/renamefield/last";
		File[] tests = Util.getTestsFromPath(path);

		int totalOfProgNotRefactByNetbeans = 0;
		int totalOfPrograms = 0;
		
		for (File test : tests) {
			
			totalOfProgNotRefactByNetbeans += checkIfWasNotRefactByNetbeans(test);
			
			totalOfPrograms++;
			
		}
		
		printTotalOfPrograms(totalOfPrograms);
		
		printTotalOfProgNotRefactByNetbeans(totalOfProgNotRefactByNetbeans);
	}

	/**The return type is int because this must be used to increase an int variable to count
	 * how many programs was not refactored by Netbeans. */
	private static int checkIfWasNotRefactByNetbeans(File test){
		File outNetBeans = new File(test,"out/netbeans");
		int answer = 0;
		if ( !outNetBeans.exists())
			answer = 1;
		return answer;
	}
	
	private static void printTotalOfProgNotRefactByNetbeans(int count) {
		System.out.println("programs n�o refatorados pelo netbeans: " + count);
	}

	private static void printTotalOfPrograms(int i) {
		System.out.println("total de programas: " + i);
	}
}

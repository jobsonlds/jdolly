package jdolly.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MoveResults {

	public static void main(String[] args) {
		run();
	}

	private static void run() {
		String pathSource = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/pullupfieldjrrt/last";
		String pathTarget = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/pullupfield/last";

		File[] tests = Util.getTestsFromPath(pathSource);
		
		int totalOfCopiedFiles = countHowManyFilesInTargetPathWereCopied(pathTarget, tests);
		
		printTotalOfCopiedFiles(totalOfCopiedFiles);
	}

	private static void printTotalOfCopiedFiles(int totalOfFilesCopied) {
		System.out.println("programas copiados: " + totalOfFilesCopied);
	}

	private static int countHowManyFilesInTargetPathWereCopied(String pathTarget, File[] tests) {
		int count = 0;
		
		for (File test : tests) {
			
			File source = new File(test,"out/jrrt");
			File targetDir = new File(pathTarget);
			File targetTestDir = new File(targetDir,test.getName());
			File target = new File(targetTestDir,"out/jrrt");
			
			tryToCopyDirectory(source, target);
			
			count++;

		}
		return count;
	}

	private static void tryToCopyDirectory(File source, File target) {
		try {
			copyDirectory(source, target);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {

		if (sourceLocation.isDirectory()) {
			
			createDirectoryIfNotExists(targetLocation);

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			InputStream input = new FileInputStream(sourceLocation);
			OutputStream output = new FileOutputStream(targetLocation);

			FileUtil.copyTheBitsFromInputStreamToOutputStream(input, output);
			
			input.close();
			output.close();
		}

	}

	private static void createDirectoryIfNotExists(File targetLocation) {
		if (!targetLocation.exists()) {
			targetLocation.mkdir();
		}
	}

}

package jdolly.util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.ProjectHelper;


public class Compile {

	public static String compile(String path) {
		
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		ByteArrayOutputStream outputByteArrayError = new ByteArrayOutputStream();

		PrintWriter pw = new PrintWriter(outputByteArray);
		PrintWriter pwerror = new PrintWriter(outputByteArrayError);

		org.eclipse.jdt.internal.compiler.batch.Main.compile(
				"-classpath rt.jar " + path, pw, pwerror);

		// System.out.println(out.toString());
		// System.out.println(outError.toString());
		return outputByteArrayError.toString();
	}

	public static void main(String[] args) {
		runCompileCounter();		

	}

	private static void runCompileCounter() {
		//		String path = "/private/var/folders/bx/bxpvKGfwF-yg3RjPZ5LRJk+++TI/-Tmp-/pullupfield0";
				String path = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/pullupfield/last";
				File[] tests = Util.getTestsFromPath(path);
				
				int totalOfGeneratedPrograms = tests.length;
				
				int totalOfCompilErrors = countCompilationErrors(tests);
				
				printTotalGeneratedPrograms(totalOfGeneratedPrograms);
				
				printTotalCompilationErrors(totalOfCompilErrors);
				
				printPorcentOfCompilErrors(totalOfCompilErrors, totalOfGeneratedPrograms);
	}

	private static int countCompilationErrors(File[] tests) {
		int totalOfCompilErrors = 0;
		for (File test : tests) {
			File out = new File(test,"in");
			String outputMsg = compile(out.toString());
		    
		    if (outputMsg.contains("ERROR in")) {
		    	totalOfCompilErrors++;        	
		    }
		}
		return totalOfCompilErrors;
	}
	
	private static void printPorcentOfCompilErrors(int i, int j) {
		double per = (i * 100) / j; 
		System.out.println("porcentagem:" + per);
	}

	private static void printTotalCompilationErrors(int i) {
		System.out.println("número de erros de compilação: " + i);
	}

	private static void printTotalGeneratedPrograms(int j) {
		System.out.println("total de programas gerados: " + j);
	}

}

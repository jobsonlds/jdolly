package jdolly.util;



import java.util.*;
import java.io.*;

public class FileUtil {

	public static void getClasses(String basePathOfProject, Vector<String> namesOfJavaFiles,
			String baseNameOfPackage) {
		try {
			tryToGetClasses(basePathOfProject, namesOfJavaFiles, baseNameOfPackage);
		} catch (Exception e) {
			printErrorInFileUtilMethod("getClasses()");
			e.printStackTrace();
		}
	}

	private static void printErrorInFileUtilMethod(String methodName) {
		System.err.println("Erro no metodo FileUtil." + methodName);
	}

	private static void tryToGetClasses(String path, Vector<String> result, String base) {
		
		File[] files = getFilesFromPath(path);
		int totalOfFiles = files.length;
		
		for (int pos = 0; pos < totalOfFiles; pos++) {
			File actualFile = files[pos];
			String actualFileName = actualFile.getName();
			if (actualFile.isDirectory()) {
				// adicionamos os subdiretorios
				String baseTemp = base + actualFileName + StrUtil.DOT_SYMBOL;
				getClasses(actualFile.getAbsolutePath(), result, baseTemp);
			} else {
				// so consideramos arquivos java
				boolean isJavaFile = actualFileName.endsWith(StrUtil.CLASS_EXTENSION);
				if (isJavaFile) {
					String fileNameWithJavaExtension = base + actualFileName;
					String fileNameWithoutJavaExtension = removeJavaExtension(fileNameWithJavaExtension);
					result.add(fileNameWithoutJavaExtension);
				}
			}
		}
	}
	
	private static File[] getFilesFromPath(String path) {
		File dir = new File(path);
		File[] files = dir.listFiles();
		return files;
	}

	private static String removeJavaExtension(String arquivo) {
		arquivo = arquivo.replaceAll(StrUtil.CLASS_EXTENSION, StrUtil.EMPTY_STRING);
		return arquivo;
	}

	public static void print(Vector<String> result) {
		String arquivo;
		for (int i = 0; i < result.size(); i++) {
			arquivo = result.get(i);
			System.out.print(removeJavaExtension(arquivo) + ", ");
		}
		System.out.println();
	}

	public static String leArquivo(String name) {
		String result = StrUtil.EMPTY_STRING;
		try {
			FileReader fr = new FileReader(new File(name));
			BufferedReader buf = new BufferedReader(fr);
			while (buf.ready()) {
				result += "\n" + buf.readLine();
			}
		} catch (Exception e) {
			printErrorInFileUtilMethod("leArquivo()");
			e.printStackTrace();
		}
		return result;
	}

	public static String leArquivoQuebraLinha(String name) {
		String result = StrUtil.EMPTY_STRING;
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(name));
			char[] buf = new char[1024];
			int numRead = 0;
			while ((numRead = reader.read(buf)) != -1) {
				fileData.append(buf, 0, numRead);
			}
			reader.close();
			result = fileData.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void gravaArquivo(String name, String texto) {
		try {
			FileWriter fw = new FileWriter(new File(name), false);
			fw.write(texto);
			fw.close();
		} catch (Exception e) {
			printErrorInFileUtilMethod("gravaArquivo()");
			e.printStackTrace();
		}
	}

	// testar o que foi feito
	public static void main(String[] args) {
		run();
	}

	private static void run() {
		Vector<String> result = new Vector<String>();
		String baseDir = "F:\\eclipse\\analyzer\\src";
		getClasses(baseDir, result, "");
		System.out.println("O diretorio tem " + result.size() + " arquivos.");
		print(result);
		gravaArquivo("F:\\rohit.txt", "valor 1 + ");
		System.out.println("O arquivo lido e: " + leArquivo("F:\\rohit.txt"));
	}

	public static void createFolders() throws Exception {
		String tempDir = System.getProperty("java.io.tmpdir") + "/safeRefactor";
		
		File root = new File(tempDir);
		File build = new File(tempDir + "/buildTests");
		File tests = new File(tempDir + "/tests");
		
		root.mkdir();
		build.mkdir();
		tests.mkdir();		
		
		if (!root.isDirectory() || !build.isDirectory() || !tests.isDirectory())
			throw new Exception("Error while creating temporary folders");

	}
	
	public static void copyFolder(File src, File dest)
	    	throws IOException{
	 
	    	if(src.isDirectory()){
	 
	    		//if directory not exists, create it
	    		if(!dest.exists()){
	    		   dest.mkdir();
	    		   System.out.println("Directory copied from " 
	                              + src + "  to " + dest);
	    		}
	 
	    		//list all the directory contents
	    		String files[] = src.list();
	 
	    		for (String file : files) {
	    		   //construct the src and dest file structure
	    		   File srcFile = new File(src, file);
	    		   File destFile = new File(dest, file);
	    		   //recursive copy
	    		   copyFolder(srcFile,destFile);
	    		}
	 
	    	}else{
	    		//if file, then copy it
	    		//Use bytes stream to support all file types
	    		InputStream in = new FileInputStream(src);
    	        OutputStream out = new FileOutputStream(dest); 
 
    	        copyTheBitsFromInputStreamToOutputStream(in, out);
 
    	        in.close();
    	        out.close();
    	        System.out.println("File copied from " + src + " to " + dest);
	    	}
	    }

	//this can be used in MoveResult as well
	public static void copyTheBitsFromInputStreamToOutputStream(InputStream in, OutputStream out) throws IOException {
		int length;
		byte[] buffer = new byte[1024];
		
		while ((length = in.read(buffer)) > 0){
		   out.write(buffer, 0, length);
		}
	}
}

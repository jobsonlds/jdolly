package jdolly.util;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class TSPFilter {

	//private static final String TOOL = "jrrt";

	public static void main(String[] args) {
		run();
	}

	private static void run() {
		String path = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/"
				+ StrUtil.REFACT_MOVE_METHOD + "/last/";
		File[] tests = Util.getTestsFromPath(path);

		for (File test : tests) {
			checkIfTooStrongPrecondExistsInFile(test);
		}	
		saveFileContentFromPath(path);
	}

	private static void checkIfTooStrongPrecondExistsInFile(File test) {
		File in = new File(test, "in");
		File out = new File(test, "out/" + StrUtil.JRRT_REFACTORING_TOOLS);

		File tsp = new File(out, StrUtil.TOO_STRONG);
		if (tsp.exists()) {
			File messageFile = new File(out, StrUtil.REFACT_INAPPLICABLE);
			String message = FileUtil.leArquivo(messageFile
					.getAbsolutePath());		
			message = getMessageTemplate(message);
			String program = Util.getProgram(in);
			program = test.getAbsolutePath() + "\n\n" + program;

			Precondition pre = Precondition.getNamed(message);
			
			if (pre != null) {
				List<String> programsFromPrecondition = pre.getPrograms();
				programsFromPrecondition.add(program);
				pre.setQnt(pre.getQnt() + 1);					
			} else
				Precondition.loadPrecondition(message, program);
		}
	}

	private static void saveFileContentFromPath(String path) {
		int i = 1;
		Enumeration pres = Precondition.getAll().elements();		
		while (pres.hasMoreElements()) {
			Precondition p = (Precondition) pres.nextElement();
			System.out.println(p.getMessage());
			System.out.println(p.getQnt());
			
			//SALVA PROGRAMAS
			String file = path + StrUtil.JRRT_REFACTORING_TOOLS + "_PRE" + i;
			String content = getContentProgramsOfPrecondition(p);
			FileUtil.gravaArquivo(file, content);

			Util.printSeparator();
			
			i++;
		}
	}

	private static String getContentProgramsOfPrecondition(Precondition p) {
		String content = p.getMessage() + "\n";
		List<String> programs = p.getPrograms();
		int j = 1;
		for (String program : programs) {
			content = content + "---------------\n";
			content = content + "Program " + j + "\n";
			content = content + program;
			
			j++;
		}
		return content;
	}

	private static String getMessageTemplate(String message) {
		
		String result = message.replaceAll("'\\S*", "'[]'");
		result = result.replaceAll("'\\n.*", "'");
		return result;
	}

}

package jdolly.util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BCFilter {
	
	public static void main(String[] args) {
		runBCFilter();
	}

	private static void runBCFilter() {
		
		String path = "/Users/gustavo/Doutorado/experiments/refactoring-constraints-new/encapsulatefield/last/";
		File[] tests = Util.getTestsFromPath(path);

		int totalOfBehaviorChangeFailures = countHowManyBehaviorChangeFailsExists(tests);
		System.out.println(totalOfBehaviorChangeFailures);
	}

	private static int countHowManyBehaviorChangeFailsExists(File[] tests) {
		int totalOfBehaviorChangeFailures = 0;
		String program;
		for (File test : tests) {
			File in = new File(test, "in");
			File out = new File(test, "out/jrrt");
			File bcSR1 = new File(out, "BEHAVIORCHANGE_FAILURE");
			
			if (bcSR1.exists()) {			
				program = Util.getProgram(in);
//				String program2 = Util.getProgram(out);
				//if (program.contains("Class1_0.this.k_0") && !program.contains("int a")  ) {
					System.out.println(test);
					Util.printPrograms(in, out);
					totalOfBehaviorChangeFailures++;	
				//}
			}
		}
		return totalOfBehaviorChangeFailures;
	}

}

package jdolly;

import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorWarning;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;
import jdolly.util.Util;

public final class AlloyRunJava {

	public static void main(String[] args) throws Err {
		final String theory = "alloyTheory/pullupmethodwithpkg.als";
		run(theory);
	}

	public static long run(String theory) {
		
		final A4Reporter report = new A4Reporter() {
			@Override
			public void warning(final ErrorWarning msg) {
				printRelevanceWarning(msg);
			}

			private void printRelevanceWarning(final ErrorWarning msg) {
				String msgToPrint = msg.toString();
				String msgTrimmed = msgToPrint.trim();
				
				System.out.print("Relevance Warning:\n"
						+ (msgTrimmed) + "\n\n");
				System.out.flush();
			}
		};

		printTheory(theory);
		
		int totalOfExec = tryToDefineTheTotalOfExec(theory, report);

		return totalOfExec;
	}

	private static int tryToDefineTheTotalOfExec(String theory, A4Reporter report) {
		
		int currentExec = 0;
				
		try {
			currentExec = defineTheValueOfCurrentExec(theory, report);
		} catch (Err e) {
			e.printStackTrace();
		}
		return currentExec;
	}

	private static int defineTheValueOfCurrentExec(String theory, A4Reporter report) throws Err {
		
		int currentExec = 0;
		Module world = CompUtil.parseEverything_fromFile(report, null, theory);
		A4Options options = Util.defHowExecCommands();

		for (Command command : world.getAllCommands()) {
			Util.printCurrentCommandExec(command);
			A4Solution ans = TranslateAlloyToKodkod.execute_command(report,
					world.getAllReachableSigs(), command, options);

			final long tempoAntes = System.currentTimeMillis();

			int totalAnsSatisfiable = 1;
			
			// delay
			while (ans != null) {
				printTheGeneration(currentExec);
				ans = ans.next();
				
				boolean answerIsSatisfiable = ans.satisfiable();
				if (answerIsSatisfiable){
					totalAnsSatisfiable++;
				}else{
					break;
				}
				currentExec++;
			}
			
			final long tempoDepois = System.currentTimeMillis();
			
			printTotalOfSolutions(totalAnsSatisfiable, tempoAntes, tempoDepois);
		}
		return currentExec;
	}

	private static void printTheGeneration(final int generation) {
		System.out.println("Geracao: " + generation);
	}

	private static void printTotalOfSolutions(final int cont, final long tempoAntes, final long tempoDepois) {
		System.out.println("O nœmero de solu�›es Ž: " + cont + " em "
				+ (tempoDepois - tempoAntes) / 1000 + "s");
	}

	private static void printTheory(final String theory) {
		System.out.println("=========== Parsing+Typechecking " + theory
				+ " =============");
	}
}

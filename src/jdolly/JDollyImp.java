package jdolly;

import java.util.ArrayList;
import java.util.List;
import jdolly.util.Util;
import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorSyntax;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.CommandScope;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.parser.CompUtil;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Options;
import edu.mit.csail.sdg.alloy4compiler.translator.TranslateAlloyToKodkod;

public class JDollyImp extends JDolly {

	public JDollyImp(String alloyTheory, int maxPackages, int maxClasses,
			int maxMethods, int maxFields) {
		super();
		this.alloyTheory = alloyTheory;
		this.maxPackages = maxPackages;
		this.maxClasses = maxClasses;
		this.maxClassNames = maxClasses;
		this.maxMethods = maxMethods;
		this.maxMethodNames = maxMethods;
		this.maxMethodBody = maxMethods;
		this.maxFields = maxFields;
		this.maxFieldNames = maxFields;

	}

	public JDollyImp(String alloyTheory, Integer maxPackages,
			int maxClasses, int maxMethods) {
		super();
		this.alloyTheory = alloyTheory;
		this.maxPackages = maxPackages;
		this.maxClasses = maxClasses;
		this.maxClassNames = maxClasses;
		this.maxMethods = maxMethods;
		this.maxMethodNames = maxMethods;
		this.maxMethodBody = maxMethods;
	}

	public JDollyImp(String alloyTheory) {
		super();
		this.alloyTheory = alloyTheory;

	}

	private ConstList<CommandScope> createScopeList() throws ErrorSyntax {

		List<CommandScope> result = new ArrayList<CommandScope>();

		Sig type = createSigByName("Class");
		Sig method = createSigByName("Method");
		Sig methodId = createSigByName("MethodId");
		Sig classId = createSigByName("ClassId");
		Sig package_ = createSigByName("Package");
		Sig body = createSigByName("Body");

		CommandScope packageScope = new CommandScope(package_,
				isExactMaxPackages, maxPackages);
		result.add(packageScope);

		CommandScope typeScope = new CommandScope(type, isExactMaxClasses(),
				maxClasses);
		result.add(typeScope);

		CommandScope classIdScope = new CommandScope(classId,
				isExactMaxClassNames, maxClassNames);
		result.add(classIdScope);

		CommandScope methodScope = new CommandScope(method, isExactMaxMethods,
				maxMethods);
		result.add(methodScope);

		CommandScope methodIdScope = new CommandScope(methodId,
				isExactMaxMethodNames, maxMethodNames);
		result.add(methodIdScope);

		CommandScope bodyScope = new CommandScope(body, isExactMethodBodyScope,
				maxMethodBody);
		result.add(bodyScope);

		
		addFieldScopeToResult(result);
		
		return ConstList.make(result);
	}

	private void addFieldScopeToResult(List<CommandScope> result) throws ErrorSyntax {
		final Sig field = createSigByName("Field");
		final Sig fieldId = createSigByName("FieldId");
		if (this.maxFields != null) {
			CommandScope fieldScope = new CommandScope(field, isExactMaxFields,
					maxFields);
			result.add(fieldScope);

			CommandScope fieldIdScope = new CommandScope(fieldId,
					isExactMaxFieldnames, maxFieldNames);
			result.add(fieldIdScope);
		}
	}

	@Override
	protected void initializeAlloyAnalyzer() {
		
		// Alloy4 sends diagnostic messages and progress reports to the A4Reporter.
		A4Reporter report = createA4Reporter();

		try {
			tryToExecTheCommands(report);
		} catch (Err e) {
			e.printStackTrace();
		}
	}

	private void tryToExecTheCommands(A4Reporter rep) throws Err, ErrorSyntax {
		javaMetamodel = CompUtil.parseEverything_fromFile(rep, null,
				alloyTheory);

		ConstList<CommandScope> constList;
		A4Options options;
		Command commandChanged;
		
		for (Command command : javaMetamodel.getAllCommands()) {

			constList = createScopeList();

			commandChanged = command.change(constList);

			options = Util.defHowExecCommands();

			Util.printCurrentCommandExec(commandChanged);

			currentAns = TranslateAlloyToKodkod.execute_command(rep,
					javaMetamodel.getAllReachableSigs(), commandChanged, options);
		}
	}

	

	

}

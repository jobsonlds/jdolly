package jdolly;

import java.util.List;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.testorrery.Generator;

import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorWarning;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import jdolly.util.StrUtil;

public abstract class JDolly extends Generator<List<CompilationUnit>> {

	AlloyToJavaTranslator extractor;

	private int currentProgram = 0;

	protected A4Solution currentAns;

	private boolean isFirstTime = true;

	protected String alloyTheory;

	protected int maxPackages;

	protected int maxClasses;

	protected int maxClassNames;

	protected int maxMethods;

	protected int maxMethodNames;

	protected boolean isExactMaxClasses = false;

	protected Module javaMetamodel;

	protected boolean isExactMaxClassNames = false;

	protected boolean isExactMaxPackages = false;

	protected boolean isExactMaxMethods = false;

	protected boolean isExactMaxMethodNames = false;

	protected boolean isExactMethodBodyScope = false;

	protected int maxMethodBody;

	protected Integer maxFields;

	protected boolean isExactMaxFields = false;

	protected boolean isExactMaxFieldnames = false;

	protected int maxFieldNames;
	
	protected abstract void initializeAlloyAnalyzer();

	@Override
	protected List<CompilationUnit> generateNext() {
		currentProgram++;
		printCurrentProgram();

		extractor = new AlloyToJavaTranslator(currentAns);
		
		List<CompilationUnit> result = extractor.getJavaCode();

		return result;
	}

	private void printCurrentProgram() {
		System.out
				.println("Program "
						+ currentProgram);
	}

	@Override
	public boolean hasNext() {
		
		if (isFirstTime) {
			initializeAlloyAnalyzer();
		}
		boolean isCurrentAnsSatisfiable = currentAns.satisfiable();
		if (isCurrentAnsSatisfiable && isFirstTime) {			
			isFirstTime = false;
			return true;
		}
		boolean isCurrentProgramTheLast = maximumPrograms == currentProgram;
		if (maximumPrograms > 0 && isCurrentProgramTheLast){ 
			return false;
		}
		boolean result = tryToValidateCurrentAns();
		return result;
	}
	
	private boolean tryToValidateCurrentAns() {
		boolean result = true;
		try {
			if (!currentAns.next().satisfiable() || currentAns.equals(currentAns.next()))
				result = false;
			
			currentAns = currentAns.next();
			
		} catch (Err e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	public int getMaxClassNames() {
		return maxClassNames;
	}

	public void setMaxClassNames(int maxClassNames) {
		this.maxClassNames = maxClassNames;
	}

	public boolean isExactMaxClasses() {
		return isExactMaxClasses;
	}

	public void setExactMaxClasses(boolean isExactMaxClasses) {
		this.isExactMaxClasses = isExactMaxClasses;
	}

	public int getMaxMethodNames() {
		return maxMethodNames;
	}

	public void setMaxMethodNames(int maxMethodNames) {
		this.maxMethodNames = maxMethodNames;
	}

	public boolean isExactMaxClassNames() {
		return isExactMaxClassNames;
	}

	public void setExactMaxClassNames(boolean isExactMaxClassNames) {
		this.isExactMaxClassNames = isExactMaxClassNames;
	}

	public boolean isExactMaxPackages() {
		return isExactMaxPackages;
	}

	public void setExactMaxPackages(boolean isExactMaxPackages) {
		this.isExactMaxPackages = isExactMaxPackages;
	}

	public boolean isExactMaxMethods() {
		return isExactMaxMethods;
	}

	public void setExactMaxMethods(boolean isExactMaxMethods) {
		this.isExactMaxMethods = isExactMaxMethods;
	}

	public boolean isExactMaxMethodNames() {
		return isExactMaxMethodNames;
	}

	public void setExactMaxMethodNames(boolean isExactMaxMethodNames) {
		this.isExactMaxMethodNames = isExactMaxMethodNames;
	}

	public boolean isExactMethodBodyScope() {
		return isExactMethodBodyScope;
	}

	public void setExactMethodBodyScope(boolean isExactMethodBodyScope) {
		this.isExactMethodBodyScope = isExactMethodBodyScope;
	}

	public int getMaxMethodBody() {
		return maxMethodBody;
	}

	public void setMaxMethodBody(int maxMethodBody) {
		this.maxMethodBody = maxMethodBody;
	}

	public boolean isExactMaxFields() {
		return isExactMaxFields;
	}

	public void setExactMaxFields(boolean isExactMaxFields) {
		this.isExactMaxFields = isExactMaxFields;
	}

	public boolean isExactMaxFieldnames() {
		return isExactMaxFieldnames;
	}

	public void setExactMaxFieldnames(boolean isExactMaxFieldnames) {
		this.isExactMaxFieldnames = isExactMaxFieldnames;
	}

	public int getMaxFieldNames() {
		return maxFieldNames;
	}

	public void setMaxFieldNames(int maxFieldNames) {
		this.maxFieldNames = maxFieldNames;
	}

	protected A4Reporter createA4Reporter() {
		return new A4Reporter() {
			@Override
			public void warning(ErrorWarning msg) {
				String msgToBeIssued = msg.toString();
				printRelevanceWarning(msgToBeIssued);
			}
			private void printRelevanceWarning(String msgToBeIssued) {
				System.out.print("Relevance Warning:\n"
						+ (msgToBeIssued.trim()) + "\n\n");
				System.out.flush();
			}
		};
	}
	
	protected Sig createSigByName(String sigName) {
		Sig result = null;
		String labelOfSig = "";
		ConstList<Sig> reachableSigs = javaMetamodel.getAllReachableSigs();
		for (Sig sig : reachableSigs) {
			labelOfSig = replaceCrapInSigsLabel(sig);
			if (labelOfSig.equals(sigName))
				result = sig;
		}

		return result;
	}

	private String replaceCrapInSigsLabel(Sig sig) {
		String labelOfSig;
		labelOfSig = sig.label;
		labelOfSig = labelOfSig.replaceAll(StrUtil.REGEX_CRAP_SYMBOLS, StrUtil.EMPTY_STRING);
		return labelOfSig;
	}

	public A4Solution getInstance(int i) {
		return currentAns;
	}
	
	

	

}
